;;;--------------------------------------------------------
;;;
;;;---------------- DEFINITION DE LA BASE DE REGLES ----------------------
;;;
;;;--------------------------------------------------------

(defparameter *baseDeRegles*
  	 '((R37 ((eq but remise_forme))(eq obj remise_forme))
    	(R38 ((eq but competition))(eq obj competition))
    	(R1 ((eq sexe homme)(< ta 170)) (eq taille petit))
    	(R2 ((eq sexe homme)(>= ta 170) (< ta 185)) (eq taille moyen))
      (R3 ((eq sexe homme)(>= ta 185)) (eq taille grand))
      (R55 ((eq sexe femme)(< ta 150)) (eq taille petit))
    	(R56 ((eq sexe femme)(>= ta 150) (< ta 175)) (eq taille moyen))
    	(R57 ((eq sexe femme)(>= ta 175)) (eq taille grand))
    	(R4 ((eq taille petit) (> poids 75)) (eq IMC surpoids))
    	(R5 ((eq taille petit) (>= poids 53) (<= poids 75)) (eq IMC normale))
    	(R7 ((eq taille petit) (< poids 53)) (eq IMC souspoids))
    	(R8 ((eq taille moyen) (> poids 85)) (eq IMC surpoids))
    	(R9 ((eq taille moyen) (>= poids 58) (<= poids 85))(eq IMC normale))
    	(R10 ((eq taille moyen) (< poids 58)) (eq IMC souspoids))
    	(R11 ((eq taille grand) (> poids 95)) (eq IMC surpoids))
    	(R12 ((eq taille grand) (>= poids 68) (<= poids 95))(eq IMC normale))
    	(R13 ((eq taille grand) (< poids 68)) (eq IMC souspoids))
    	(R14 ((>= dvc 90)) (eq puissance_haut fort))
    	(R15 ((< dvc 90)(>= dvc 65)) (eq puissance_haut normale))
    	(R16 ((< dvc 65)) (eq puissance_haut faible))
    	(R17 ((>= squat 80)) (eq puissance_bas fort))
    	(R18 ((< squat 80)(>= squat 55)) (eq puissance_bas normale))
    	(R19 ((< squat 55)) (eq puissance_bas faible))
    	(R20 ((>= vma 16)) (eq endurance fort))
    	(R21 ((< vma 16)(>= vma 13)) (eq endurance normale))
    	(R22 ((< vma 13)) (eq endurance faible))
    	(R23 ((eq caractere1 agressif))(eq type_sport contact))
    	(R24 ((eq caractere1 esprit_equipe))(eq type_sport collectif))
    	(R25 ((eq caractere1 strategique))(eq type_sport strat�gique))
    	(R26 ((eq caractere1 solitaire))(eq type_sport individuelle))
    	(R27 ((eq caractere1 mentale))(eq type_sport mentale)) ;;sang froid
    	(R28 ((eq caractere1 artistique))(eq type_sport art))
    	(R35 ((eq caractere1 nature))(eq type_sport nature))
    	(R29 ((eq caractere2 agressif))(eq type_sport2 contact))
    	(R30 ((eq caractere2 esprit_equipe))(eq type_sport2 collectif))
    	(R31 ((eq caractere2 strategique))(eq type_sport2 strat�gique))
    	(R32 ((eq caractere2 solitaire))(eq type_sport2 individuelle))
    	(R33 ((eq caractere2 mentale))(eq type_sport2 mentale)) ;;sang froid
    	(R34 ((eq caractere2 artistique))(eq type_sport2 art))
    	(R36 ((eq caractere2 nature))(eq type_sport2 nature))
    	(R39 ((eq obj remise_forme)(eq IMC surpoids)(eq caractere1 solitaire)(eq caractere2 artistique))(equal sport "sport de d�tente et relaxation : tai chi, yoga, gymnastique douce"))
    	(R40 ((eq obj remise_forme)(eq IMC normale)(eq caractere1 esprit_equipe)(eq caractere2 nature))(equal sport "sport collectif en pleine air "))
    	(R52 ((eq obj remise_forme)(eq IMC faible)(eq caractere1 solitaire)(eq caractere2 agressif))(equal sport "musculation, renforcement musculaire"))
      (R48 ((eq obj remise_forme)(eq IMC normale)(eq caractere1 esprit_equipe)(eq caractere2 artistique))(equal sport "danse en couple"))
      (R54 ((eq obj remise_forme)(eq caractere1 strategique)(eq caractere2 solitaire))(equal sport "�checs"))
    	(R41 ((eq obj competition)(eq IMC surpoids)(eq puissance_bas fort)(eq puissance_haut fort)(eq endurance normale)(eq caractere1 agressif)(eq caractere2 esprit_equipe))(equal sport "rugby, football am�ricain"))
    	(R50 ((eq obj competition)(eq IMC surpoids)(eq puissance_bas fort)(eq puissance_haut fort)(eq endurance normale)(eq caractere1 agressif)(eq caractere2 solitaire))(equal sport "lutte, judo"))
    	(R42 ((eq obj competition)(eq IMC moyen)(eq puissance_bas fort)(eq puissance_haut fort)(eq endurance fort)(eq caractere1 agressif)(eq caractere2 solitaire))(equal sport "boxe, karat�"))
    	(R43 ((eq obj competition)(eq taille grand)(eq puissance_bas fort)(eq puissance_haut normale)(eq endurance normale)(eq caractere1 esprit_equipe)(eq caractere2 strategique))(equal sport "volley-ball, basket-ball"))
    	(R44 ((eq obj competition)(eq taille moyen)(eq IMC normale)(eq endurance faible)(eq puissance_bas faible)(eq puissance_haut faible)(eq caractere1 solitaire)(eq caractere2 mentale))(equal sport "golf"))
    	(R45 ((eq obj competition)(eq taille petit)(eq IMC normale)(eq endurance faible)(eq puissance_bas faible)(eq puissance_haut faible)(eq caractere1 solitaire)(eq caractere2 mentale))(equal sport "equitation"))
    	(R46 ((eq obj competition)(eq taille grand)(eq IMC normale)(eq endurance fort)(eq puissance_bas normale)(eq puissance_haut normale)(eq caractere1 solitaire)(eq caractere2 mentale))(equal sport "tennis, badminton"))
    	(R47 ((eq obj competition)(eq IMC faible)(eq endurance fort)(eq puissance_bas faible)(eq puissance_haut faible)(eq caractere1 mentale)(eq caractere2 solitaire))(equal sport "coureur de fond"))
    	(R49 ((eq obj competition)(eq IMC normale)(eq endurance fort)(eq puissance_bas normale)(eq puissance_haut normale)(eq caractere1 esprit_equipe)(eq caractere2 mentale))(equal sport "football"))
    	(R53 ((eq obj competition)(eq taille petit)(eq IMC normale)(eq endurance normale)(eq puissance_bas fort)(eq puissance_haut fort)(eq caractere1 solitaire)(eq caractere2 artistique))(equal sport "gymnastique"))
    	(R51 ((eq obj competition)(eq IMC normale)(eq endurance normale)(eq puissance_bas normale)(eq puissance_haut normale)(eq caractere1 solitaire)(eq caractere2 artistique))(equal sport "patinage artistique"))
      ))


;;;--------------------------------------------------------
;;;
;;;---------------- FONCTIONS DE SERVICE SIMPLES ----------------------
;;;
;;;--------------------------------------------------------

;;Conclusion d'une r�gle
(defun cclRegle (regle) (caddr regle))
;;Premisses d'une r�gle
(defun premisseRegle (regle) (cadr regle))
; Numero d'une r�gle
(defun numRegle (regle) (car regle))

(cclRegle '(R51 ((eq obj competition)(eq IMC normale)(eq vma normale)(eq puissance_bas normale)(eq puissance_haut normale)(eq caractere1 solitaire)(eq caractere2 artistique))(sport "patinage artistique")))
(premisseRegle  '(R51 ((eq obj competition)(eq IMC normale)(eq vma normale)(eq puissance_bas normale)(eq puissance_haut normale)(eq caractere1 solitaire)(eq caractere2 artistique))(sport "patinage artistique")))
(numRegle '(R51 ((eq obj competition)(eq IMC normale)(eq vma normale)(eq puissance_bas normale)(eq puissance_haut normale)(eq caractere1 solitaire)(eq caractere2 artistique))(sport "patinage artistique")))


;;;--------------------------------------------------------
;;;
;;;---------------- INITIALISATION DE LA BASE DE FAITS EN REPONDANT A DIFFERENTE QUESTION ----------------------
;;;
;;;--------------------------------------------------------
(defun initialisation ()
  (let ((reponse nil)(bdf nil))
    ;; objectif
    (format t "~%Objectif sportif fix� (chiffre � rentrer) :~%1. Remise en forme (d�tente)~%2. Competition (sport actif)~%")
    (format t "Votre choix : ")
    (setq reponse (read))
    (case reponse
      (1 (push (list 'but 'remise_forme) bdf))
      (2 (push (list 'but 'competition) bdf)
       ; si l'objectif est competition alors on pose une suite de questions sur les caract�res physiques
       ;; sexe
       (format t "~%Votre sexe :~%1. Femme~%2. Homme~%")
       (format t "Votre choix : ")
       (setq reponse (read))
       (case reponse
         (1 (push (list 'sexe 'femme) bdf))
         (2 (push (list 'sexe 'homme) bdf)))
       ;; taille
       (format t "~%Taille en cm (chiffre � rentrer) : ")
       (setq reponse (read))
       (push (list 'ta reponse) bdf)
       ;; poids
       (format t "~%Poids en kg (chiffre � rentrer) : ")
       (setq reponse (read))
       (push (list 'poids reponse) bdf)
       ;; dvc
       (format t "~%Poids maximal soulev� au d�velopp� couch� (chiffre � rentrer) : ")
       (setq reponse (read))
       (push (list 'dvc reponse) bdf)
       ;; squat
       (format t "~%Poids maximal utilis� pour effectuer des squats (chiffre � rentrer) : ")
       (setq reponse (read))
       (push (list 'squat reponse) bdf)
       ;; vma
       (format t "~%R�sultat au test de VMA (chiffre � rentrer) : ")
       (setq reponse (read))
       (push (list 'vma reponse) bdf)))
    ;; caract�re 1
    (format t "~%Vous vous d�finiriez plut�t comme (chiffre � rentrer) :~%1. Agressif~%2. Aime travailler en �quipe~%3. Strat�gique~%4. Solitaire~%5. Fort mentalement~%6. Artistique~%7. Proche de la nature~%")
    (format t "Votre choix : ")
    (setq reponse (read))
    (case reponse
      (1 (push (list 'caractere1 'agressif) bdf))
      (2 (push (list 'caractere1 'esprit_equipe) bdf))
      (3 (push (list 'caractere1 'strategique) bdf))
      (4 (push (list 'caractere1 'solitaire) bdf))
      (5 (push (list 'caractere1 'mentale) bdf))
      (6 (push (list 'caractere1 'artistique) bdf))
      (7 (push (list 'caractere1 'nature) bdf)))
    ; caract�re 2
    (format t "Votre deuxi�me choix : ")
    (setq reponse (read))
    (case reponse
      (1 (push (list 'caractere2 'agressif) bdf))
      (2 (push (list 'caractere2 'esprit_equipe) bdf))
      (3 (push (list 'caractere2 'strategique) bdf))
      (4 (push (list 'caractere2 'solitaire) bdf))
      (5 (push (list 'caractere2 'mentale) bdf))
      (6 (push (list 'caractere2 'artistique) bdf))
      (7 (push (list 'caractere2 'nature) bdf)))
    ; renvoie la base de faits mise � jour
    bdf))

;;;--------------------------------------------------------
;;;
;;;---------------- CHAINAGE AVANT ----------------------
;;;
;;;--------------------------------------------------------

; Fonction verifie_regle qui va v�rifier que toutes les premisses d'une r�gle sont v�rifi�es
; Pour chaque pr�misse on va v�rifier que
; --- si il existe une valeur associ�e � la variable de la premisse alors on v�rifie que cette valeur v�rifie la condition
; ------- si la valeur est v�rifi� alors ok reste � true
; ------- sinon ok passe � nil
; --- sinon ok passe � nil
(defun verifie_regle (demande bdf)
  (let ((ok t))
  (dolist (x (premisseRegle demande) ok)
    (let ((valeur (cadr (assoc (cadr x) bdf))))
      (if valeur
            (if (not (funcall (car x) valeur (caddr x))) (setq ok nil))
        (setq ok nil))))))

(verifie_regle '(R38 ((eq but competition))(eq obj competition)) '((but competition)))
(verifie_regle '(R15 ((< dvc 100)(>= dvc 75)) (eq puissance_haut normale)) '((dvc 75)))

;; Fonction regles_restantes
;;; R�cup�ration de toutes les r�gles qui n'ont pas encore �t� parcourues (en fonction du chemin donn� en param�tre)
(defun regles_restantes(bdr chemin) 
  (let ((c nil))
    (dolist (x bdr c)
      (if (not (member (numRegle x) chemin))
          (push x c)))))

(verifie_regle (car(reverse (regles_restantes *baseDeRegles* nil))) '((but remise_forme)))

; Fonction chainage_avant en largeur d'abord
(defun chainage_avant (bdr bdf parcouru)
  ; initialisation avec le chemin d�j� parcouru (r�gles d�j� prouv�es) et les regles restantes non prouv�es
  (let ((chemin parcouru) (candidates (reverse(regles_restantes bdr parcouru))))
    ;pour chaque r�gle restantes
    (dolist (x candidates bdf)
      ; verifie si la regle est verifiee en appelant verifie_regle
      (if (verifie_regle x bdf)
        ; si la r�gle est bien v�rifi�e
          (progn
            ; on met la conclusion de cette r�gle dans la bdf
            (push (cdr(cclRegle x)) bdf)
            ; on place la regle dans la liste de celles parcourues
            (push (numRegle x) chemin)
            ; affichage pour mieux comprendre l'�volution
            (format t "~%BDF : ~A~%" bdf)
            (format t "~%Regles parcourues : ~A~%" chemin))
        )
      )
    ; v�rifie si il n'y a plus de regles prouv�es (chemin parcouru toujours le m�me)
    (if (eq parcouru chemin)
      ; si c'est le cas on retourne la base de faits
      bdf
      ; sinon on recommence le chainage avant avec la nouvelle base de faits et le nouveau chemin parcouru
      (chainage_avant bdr bdf chemin) ;continue le chainage avant 
      )
    )
  )

;;;--------------------------------------------------------
;;;
;;;---------------- CHAINAGE ARRIERE ----------------------
;;;
;;;--------------------------------------------------------

;; Fonction regles_candidates qui retourne les r�gles candidates qui ont pour conclusion le but donn� en argument
(defun regles_candidates (but bdr) ;; (>= dvc 90) 
  ; v�rifie que la base de r�gles n'est pas vide
  (if (null bdr) ()
    ; initialisation avec la conclusion de la r�gle, l'attribut de la concusion et la valeur de la conclusion
      (let* ((conclusion (cclRegle (car bdr)))
             (attribut (cadr conclusion))
             (valeur (caddr conclusion)))
        (if 
          (and 
           ;; il faut verifier l'identit� des attributs
           (eq attribut (cadr but))
           ;; et que la regle concluera sur une valeur correcte 
           (funcall (car but) valeur (caddr but)))
            ;; alors on push la r�gle en rappelant regles_candidates sur le reste de la base de r�gles
            (cons (car bdr) (regles_candidates but (cdr bdr)))
          ;;sinon on ne fait rien et on rappelle regles_candidates sur le reste de la base de r�gles
          (regles_candidates but (cdr bdr))))))

;; Fonction appartient qui d�termine si un but est v�rifi�
;; but est de la forme: (comparateur attribut valeur)
(defun appartient (but bdf)
  ;; r�cup�re la valeur de l'attribut de but dans la base de faits
  (let ((valeur (cadr (assoc (cadr but) bdf))))
    ; v�rifie que l'op�ration de comparaison en utlisant l'op�rateur de but avec cette valeur et la valeur du but
    (funcall (car but) valeur (caddr but))))

(appartient '(equal sport "gymnastique") '((sport "gymnastique")))

;; Fonction verifier_ou (on regarde si une des regles candidates du but donn� en param�tre est verifiee)
(defun verifier_ou (but bdF bdR &optional (i 0))
  ; si le but "appartient" � la base de faits (valeur v�rifi�e)
  (if (appartient but bdF)
      ; alors le but est prouv�, on retourne vrai
      (progn 
        (format t "~V@t But : ~A proof ~%" i but)
        T)
    ; sinon on v�rifie que l'une des r�gles candidates (qui a pour conclusion le but) est v�rifi�e 
    ; (s'arr�te de boucler quand on trouve une prouv�e)
    ; si toutes les conditions d'une r�gle candidates ne sont pas v�rifi�es alors on retourne nil
    ; sinon on retourne vrai (et on sort de la boucle : n'�tudie pas les autres)
    (let ((regles (reverse (regles_candidates but bdR))) (ok nil))
     (while (and regles (not ok))
       (format t "~% ~V@t VERIFIE_OU ~A Regles ~s :  ~A ~%" i but (numRegle (car regles)) (car regles))
       (setq ok (verifier_et (pop regles) bdF bdR i)))
     ok)
    ))

;; Fonction verifier_et (on v�rifie que les premisses de la regle pass�e en param sont vraies)
(defun verifier_et (regle bdF bdR i)
  (let ((ok t) (premisses (premisseRegle regle)))
    ; boucle tant qu'il existe des pr�misses non v�rifi�es et que les pr�c�dentes ont �t� prouv�es
    ; v�rifie que chaque chaque premisse est v�rifi�e par au moins une r�gle candidate qui a pour but cet �l�ment
    ; si toutes les premisses ont �t� prouv�es alors on retourne vrai
    ; sinon (au moins une qui est non prouv�e) on retourne nil
    (while (and premisses ok)
      (format t "~V@t  ~t VERIFIE_ET ~s premisse ~A~%" (+ 1 i) (numRegle regle) (car premisses))
      (setq ok (verifier_ou (pop premisses) bdF bdR (+ 6 i))))
    ok))

; Fonction chainage_arriere en profondeur d'abord
(defun chainage_arriere (bdr bdf)
  (let ((cherche nil)(reponse nil))
    ; demande � l'utilisateur le sport dont il veut tester la compatibilit� (le but)
    (format t "~%Quel sport voulez-vous tester :~%1. Rugby, football am�ricain~%2. Lutte, judo~%3. Boxe, karat�~%4. Volley-ball, basket-ball~%5. Golf~%6. Equitation~%7. Tennis, badminton~%8. Coureur de fond~%9. Football~%10. Gymnastique~%11. Patinage artistique~%")
    (format t "Votre choix : ")
    (setq reponse (read))
    (case reponse
      (1 (setq cherche (cclRegle (assoc 'R41 *baseDeRegles*))))
      (2 (setq cherche (cclRegle (assoc 'R50 *baseDeRegles*))))
      (3 (setq cherche (cclRegle (assoc 'R42 *baseDeRegles*))))
      (4 (setq cherche (cclRegle (assoc 'R43 *baseDeRegles*))))
      (5 (setq cherche (cclRegle (assoc 'R44 *baseDeRegles*))))
      (6 (setq cherche (cclRegle (assoc 'R45 *baseDeRegles*))))
      (7 (setq cherche (cclRegle (assoc 'R46 *baseDeRegles*))))
      (8 (setq cherche (cclRegle (assoc 'R47 *baseDeRegles*))))
      (9 (setq cherche (cclRegle (assoc 'R49 *baseDeRegles*))))
      (10 (setq cherche (cclRegle (assoc 'R53 *baseDeRegles*))))
      (11 (setq cherche (cclRegle (assoc 'R51 *baseDeRegles*)))))
    (format t "~%But ~A~%" cherche)
    (format t "~%V�rification...~%")
    ; lance la v�rification du but
    ; retourne vrai si le but est prouv�, sinon retourne nil
    (return-from chainage_arriere (verifier_ou cherche bdf bdr))
    ))


;;;--------------------------------------------------------
;;;
;;;---------------- UTILISATION DU SYSTEME EXPERT ----------------------
;;;
;;;--------------------------------------------------------

;; Fonction principale qui sert de Menu
(defun systeme_expert ()
  (let ((votreChoix 0))
    (format t "~%___ Bienvenue dans notre simulation ___~%")
    (format t "~%~1@t Vous allez d'abord devoir passer une s�rie de tests afin d'avoir une expertise optimale~%")
    (format t "~%~2@t ==== D�but des tests ====~%~%")
    (let   ((bdf (initialisation))(trouve nil))
      (format t "~%~%~2@t ==== Fin des tests ====~%")
      ; v�rifie que l'utilisateur entre un choix possible
      (while (AND (not (equal votreChoix 1))(not (equal votreChoix 2)))
        (format t "~%~1@tMENU~%")
        (format t "~%Choix 1 : D�couvrir le sport qui te correspond~%")
        (format t "~%Choix 2 : Savoir si un sport pr�cis te convient (si competition)~%")
        (format t "~%Votre choix : ")
        (setq votreChoix (read))
        (case votreChoix
          (1 ; chainage-avant utilis� pour d�couvrir le sport qui correspond au profil
           (setq bdf (chainage_avant *baseDeRegles* bdf nil))
           (format t "~%BDF : ~A~%" bdf)
           ; r�ponse du syst�me
           (if (cdr(assoc 'sport bdf))
               (format t "~%Voici le r�sultat obtenu par notre syst�me : ~s~%~%" (cadr(assoc 'sport bdf)))
             (format t "~%Notre syst�me n'a pas trouv� un sport en particulier, il semble que tu pourrais tout essayer~%~%")))
          (2 ; chainage-arri�re pour tester la compatibiit� avec un sport choisi par l'utilisateur
           (setq trouve (chainage_arriere *baseDeRegles* bdf))
           ; r�ponse du syst�me
           (if trouve 
               (format t "~%Il semblerait que tu es n�(e) pour faire ce sport~%~%")
             (format t "~%Notre syst�me n'a pas rep�r� une grande compl�mentarit� avec ce sport, mais tu peux toujours essayer~%~%")))
          (T (format t "~%Erreur dans la saisie : veuillez choisir un choix propos�~%"))))
      (return-from systeme_expert "___ Merci d'avoir utiliser notre syst�me, � bient�t ___"))))

(systeme_expert)


;;;--------------------------------------------------------
;;;
;;;---------------- SCENARIOS D'UTILISATION ----------------------
;;;
;;;--------------------------------------------------------

;; _______ CAS 1 : sport pour comp�tition trouv� _______

; R�ponses aux questions : 2 (comp�tition) / 2 (homme) / 190 (taille) / 80 (poids) / 70 (d�velopp� couch�) / 82 (squats) / 15 (VMA) / 2 (esprit d��quipe) / 3 (strat�gique)
; A tester pour le choix 1 et le choix 2 (en choisissant "Volley ball, basketball" : choix 4) du Menu (chainage avant et chainage arri�re)

;; _______ CAS 2 : sport pour remise en forme trouv� _______

; R�ponses aux questions : 1 (remise en forme) / 3 (strat�gique) / 4 (solitaire)
; A tester seulement avec le choix 1 du Menu (un sport pour remise en forme ne fait pas parti du chainage arri�re car ne parait pas pertinent)

;; _______ CAS 3 : aucun sport trouv� _______

; R�ponses aux questions : 2 (comp�tition) / 2 (homme) / 170 (taille) / 66 (poids) / 60 (d�velopp� couch�) / 55 (squats) / 14 (VMA) / 6 (artistique) / 7 (proche de la nature)
; A tester pour le choix 1 et le choix 2 (en choisissant n'importe quel sport) du Menu (chainage avant et chainage arri�re)

    
    
