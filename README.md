# Expert System - Find your perfect sport

School project that aims to practice system expert.

We chose to create an expert system to find the perfect match between a person and a sport. As a sportman, I truly think that sport is essential to a good lifestyle. It's why I create this expert system to find the perfect sport that will fit us.

I coded in LISP.
